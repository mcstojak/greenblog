Zadanie:
Stwórz prostą aplikację opartą o: ReactJs, Redux, Webpack, CSS Modules oraz ES6+

Temat: Przykładowy blog
API
https://jsonplaceholder.typicode.com/

Działanie: Na stronie głównej wyświetlamy listę artykułów z odnośnikiem do pełnej wersji. Artykuł zawiera pełny opis, listę komentarzy. Dodatkowo użytkownik ma możliwość dodania komentarza i artykułu do ulubionych.

Wymagane:

standard ES6+
zmienne ‘let’ oraz ‘const’
destrukturyzacja
async/await
arrow functions
fetch API
moduły
zastosowanie biblioteki react-router do obsługi routingu i redux do zarządzania stanem aplikacji
CSS3 w notacji BEM


Dodatkowe punkty:

testy z zastosowanie biblioteki Jest
brak użycia bibliotek do stylowania
brak użycia create-react-app
nietuzinkowy UI
