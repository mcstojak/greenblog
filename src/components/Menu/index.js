import React from "react";
import menu from "../../public/css/menu.module.css";
import {NavLink} from "react-router-dom";

const Menu = () =>{
    return(
        <div className={menu.menu}>
            <h1 className={menu.logo}>GreenBlog</h1>
            <NavLink className={menu.navItem} to={`/`}>Main</NavLink>
          <NavLink className={menu.navItem} to={`/posts`}>Posts</NavLink>
        <NavLink className={menu.navItem} to={`/favorites`}>Favorites</NavLink>
          </div>
    )
}
export default Menu;