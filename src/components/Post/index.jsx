import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getUser } from "../../actions/User";
import postStyle from "../../public/css/post.module.css";
import comments from "../../public/css/comments.module.css";
import leaf from "../../public/images/leaf.png";
import whiteleaf from "../../public/images/whiteleaf.png";
import {
  showPost,
  addFavorites,
  removeFavorites,
  fetchComments,
} from "../../actions/Posts";

const Comment = ({ comment }) => {
  const { body, id, name, email } = comment;
  return (
    <div key={id} className={comments.main}>
      <h3>{name}</h3>
      <p>{body}</p>
      <p>Author: {email}</p>
    </div>
  );
};

const Post = ({ match }) => {
  const dispatch = useDispatch();
  const { id, title, body, userId, comments } = useSelector(
    (state) => state.posts.post
  );
  const { email, name } = useSelector((state) => state.users.user);
  const [like, setLike] = useState(false);
  const [hover, setHover] = useState(false);
  const favorites = useSelector((state) => state.posts.favorites);

  useEffect(() => {
    dispatch(showPost(match.params.postId));
    const fav = favorites.filter(
      (el) => el.id === parseInt(match.params.postId)
    );
    fav && fav.length > 0 ? setLike(true) : setLike(false);
  }, []);

  useEffect(() => {
    dispatch(getUser(userId));
  }, [userId]);

  useEffect(() => {
    dispatch(fetchComments(id));
  }, [id]);

  function remove(){
    setLike(false);
    dispatch(removeFavorites(parseInt(match.params.postId)));
  };
  function add(){
    setLike(true);
    dispatch(addFavorites({ id, body, title, userId, comments }));
  };
  const clickFavorites = () => {
    like ?remove():add();
  };
  return (
    <div className={postStyle.white} key={id}>
      <h2 className={postStyle.header2}>{title}</h2>
      <p className={postStyle.user}>
        {name}, {email}
      </p>
      <article className={postStyle.articleBody}>{body}</article>
      <span
        onMouseEnter={() => {
          setHover(true);
        }}
        onClick={()=>clickFavorites()}
        onMouseLeave={() => setHover(false)}
        className={like ? postStyle.favorite : postStyle.span}
      >
        <img className={postStyle.leaf} src={hover ? whiteleaf : leaf} alt="" />{" "}
        Add to favorites
      </span>

      {comments && comments.length > 0 ? (
        <div>
          <h3>{comments.length} Comments:</h3>
          {comments.map((el) => {
            return <Comment key={el.id} comment={el}></Comment>;
          })}
        </div>
      ) : (
        <div>
          <h3>Comments:</h3>
          <p>There are no comments yet</p>
        </div>
      )}
    </div>
  );
};
export default Post;
