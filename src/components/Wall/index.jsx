import React, { useEffect } from "react";
import ShortPost from "../ShortPost";
import { useSelector, useDispatch } from "react-redux";
import { showPosts } from "../../actions/Posts";
import wall from "../../public/css/wall.module.css";

const Wall = () => {
  const posts = useSelector((state) => state.posts.postsList);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(showPosts());
  }, []);
 
  return (
    <div className={wall.grid}>
      {posts &&
        posts.length > 0 &&
        posts.map((el, i) => {
          return (
            <ShortPost
              key={i}
              body={el.body}
              userId={el.userId}
              title={el.title}
              id={el.id}
            ></ShortPost>
          );
        })}
    </div>
  );
};

export default Wall;
