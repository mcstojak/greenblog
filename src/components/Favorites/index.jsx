import React from "react";
import {useSelector} from "react-redux";
import ShortPost from "../ShortPost";

const Favorites = () =>{
    const favorites = useSelector(state=>state.posts.favorites);
  
    return(
        <div>
            {favorites.length>0?favorites.map(el=>{
                return <ShortPost title={el.title} id={el.id} key={el.id} body={el.body} userId={el.userId} comments={el.comments}></ShortPost>
            }):<h1>You have no favorite posts. Read more and like some :)</h1>}
        </div>
    )
}
export default Favorites;