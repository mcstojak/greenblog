import React from "react";
import {Link} from "react-router-dom";
import main from "../../public/css/main.module.css";
import postStyle from "../../public/css/post.module.css";

const ShortPost = ({body, id, title}) =>{
  
    return(
    <div key={id} className={main.article}>
        <h3 className={postStyle.header3}>{title}</h3>
        <article className={postStyle.article}>{body.slice(0,125)}...</article>
        <Link className={postStyle.link} to={`/post/${id}`}>Read more</Link>
    </div>
    )
}

export default ShortPost;