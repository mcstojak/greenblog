import React from "react";
import main from "../../public/css/main.module.css";

const Main = () =>{
    return(
        <div className={main.transparent}>
            <div className={main.image}>
                <div className={main.centered}>
                <h1 className={main.logo}>Welcome to GreenBlog! </h1>
                <h2 className={main.header2}>Real source of climate change news</h2>
                <h2 className={main.header2}>Make a change, your voice is your choice</h2>
                </div>
            </div>
          
        
        </div>
    )
}
export default Main;