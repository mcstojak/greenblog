export const ACTION_TYPES = {
    FETCH_USER_SUCCESS: "FETCH_USER_SUCCESS",
    FETCH_USER_ERROR: "FETCH_USER_ERROR",
    CLEAR_ERROR: "CLEAR_ERROR",
  };
  
  const fetchUserSuccess = (data) => ({
    type: ACTION_TYPES.FETCH_USER_SUCCESS,
    payload: data,
  });
  
  const fetchUserError = ({err, isError}) => ({ type: ACTION_TYPES.FETCH_USER_ERROR, payload:{err, isError} });

  const clearError = (isError) => ({
    type: ACTION_TYPES.CLEAR_ERROR,
    payload: { isError: isError },
  });
  
  export const getUser = (userId) => {
    return async (dispatch) => {
          return fetch(`https://jsonplaceholder.typicode.com/users/${userId}`)
          .then(response=>response.json())
          .then(json=>{
            let user = {id: json.id, name: json.name, username:json.username, email: json.email};
            dispatch(fetchUserSuccess(user))}
            )
          .catch(err=>{
            dispatch(fetchUserError(err, true));
          });
   
    };
  };
  
  export const errorClear = () => {
    return (dispatch) => {
      dispatch(clearError(false));
    };
  };
  