export const ACTION_TYPES = {
  SET_FAVORITE_POST: "SET_FAVORITE_POST",
  UNSET_FAVORITE_POST: "UNSET_FAVORITE_POST",
  FETCH_POST_SUCCESS: "FETCH_POST_SUCCESS",
  FETCH_POSTS_SUCCESS: "FETCH_POSTS_SUCCESS",
  ADD_COMMENTS:"ADD_COMMENTS",
  FETCH_POST_ERROR: "FETCH_POST_ERROR",
  CLEAR_ERROR: "CLEAR_ERROR",
};

const fetchPostSuccess = (data) => ({
  type: ACTION_TYPES.FETCH_POST_SUCCESS,
  payload: data,
});
export const addFavorites = (favorite)=>({type: ACTION_TYPES.SET_FAVORITE_POST, payload: favorite});
export const removeFavorites = (id)=>({type:ACTION_TYPES.UNSET_FAVORITE_POST, payload: id});
const fetchPostsSuccess = (data) => ({
    type: ACTION_TYPES.FETCH_POSTS_SUCCESS,
    payload: data,
  });
const fetchPostError = (err, isError) => ({
  type: ACTION_TYPES.FETCH_POST_ERROR,
  payload: { err: err, isError: isError },
});
const addComments = (comments) =>({type: ACTION_TYPES.ADD_COMMENTS, payload:comments});
const clearError = (isError) => ({
  type: ACTION_TYPES.CLEAR_ERROR,
  payload: { isError: isError },
});

export const showPost = (id) => {
  return async (dispatch) => {
        return fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
        .then(response=>response.json())
        .then(json=>{
          dispatch(fetchPostSuccess(json))}
          )
        .catch(err=>{
          dispatch(fetchPostError(err, true));
        });
  };
};

export const fetchComments = (id) =>{
  return async (dispatch)=>{
    return fetch(`https://jsonplaceholder.typicode.com/comments?postId=${id}`)
    .then(response=>response.json())
    .then(json=>{
    dispatch(addComments(json))
  });
  }
  
}

export const showPosts = () => {
    return async (dispatch) => {
      
     return fetch(`https://jsonplaceholder.typicode.com/posts/`)
     .then((response)=>response.json())
     .then(json=>{
      dispatch(fetchPostsSuccess(json));
     })
     .catch (err=> {
        dispatch(fetchPostError(err, true));
      });
    };
  };


export const errorClear = () => {
  return (dispatch) => {
    dispatch(clearError(false));
  };
};
