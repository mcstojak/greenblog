import React, { useEffect } from "react";
import main from "./public/css/main.module.css";
import Main from "./components/Main";
import Menu from "./components/Menu";
import Wall from "./components/Wall";
import Post from "./components/Post";
import Favorites from "./components/Favorites";
import { Route } from "react-router-dom";

const App = () => {
  return (
    <div>
      <div className={main.container}>
        <Menu></Menu>
        <Route path="/post/:postId" component={Post}></Route>
        <Route path="/favorites" component={Favorites}></Route>
        <Route exact path="/posts" component={Wall}></Route>
        <Route exact path="/" component={Main}></Route>
      </div>
    </div>
  );
};

export default App;
