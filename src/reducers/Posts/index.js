import { ACTION_TYPES } from "../../actions/Posts";
const initialState = {
  favorites: [],
  postsList: [],
  post: {
    title: "",
    body: "",
    id: "",
    userId: "",
    comments: [],
  },
};

const postsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TYPES.GET_POSTS:
      return state;
    case ACTION_TYPES.CLEAR_POSTS:
      return {
        ...state,
        post: {
          ...state.post,
          title: initialState.title,
          body: initialState.body,
          id: initialState.id,
          userId: initialState.userId,
        },
        postsList: [...state.postsList, initialState.postsList],
      };

    case ACTION_TYPES.FETCH_POST_SUCCESS:
      return {
        ...state,
        post: {
          ...state.post,
          title: action.payload.title,
          body: action.payload.body,
          id: action.payload.id,
          userId: action.payload.userId,
        },
        postsList: [...state.postsList],
      };
    case ACTION_TYPES.SET_FAVORITE_POST:
      return { ...state, favorites: [...state.favorites, action.payload] };
    case ACTION_TYPES.UNSET_FAVORITE_POST:
      return {
        ...state,
        favorites: state.favorites.filter((el) => el.id !== action.payload),
      };

    case ACTION_TYPES.FETCH_POSTS_SUCCESS:
      return {
        ...state,
        postsList: [...state.postsList, ...action.payload],
      };
    case ACTION_TYPES.ADD_COMMENTS:
      return {
        ...state,
        post: { ...state.post, comments: [...action.payload] },
      };
    
    case ACTION_TYPES.FETCH_ERROR:
      return {
        ...state,
        isError: action.payload.isError,
        errorMessage: action.payload.err,
      };
    case ACTION_TYPES.CLEAR_ERROR:
      return { ...state, isError: action.payload.isError };
    default:
      return state;
  }
};
export default postsReducer;
