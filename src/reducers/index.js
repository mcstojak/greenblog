import {combineReducers} from "redux";
import postsReducer from "./Posts";
import usersReducer from "./Users";



const rootReducer = combineReducers({
    posts: postsReducer,
    users: usersReducer,
}); 
export default rootReducer;