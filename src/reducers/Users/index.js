import { ACTION_TYPES } from "../../actions/User";
const initialState = {
  loggedUser: {
    id: "",
    name: "",
    username: "",
    email: "",
  },
  users: [],
  user: {
    id: "",
    name: "",
    username: "",
    email: "",
  },
};

const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TYPES.FETCH_USER_SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          id: action.payload.id,
          name: action.payload.name,
          username: action.payload.username,
          email: action.payload.email,
        },
      };
    case ACTION_TYPES.FETCH_ERROR:
      return {
        ...state,
        isError: action.payload.isError,
        errorMessage: action.payload.err,
      };
    case ACTION_TYPES.CLEAR_ERROR:
      return { ...state, isError: action.payload.isError };
    default:
      return state;
  }
};
export default usersReducer;
